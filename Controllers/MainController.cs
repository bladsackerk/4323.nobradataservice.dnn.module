﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pervasive.Data.Common;
using Pervasive.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using DotNetNuke.Web.Api;
using System.Web.Http;
using System.Net;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.IO;
using DotNetNuke.Services.Exceptions;

using Coesolutions.Modules.NOBRADataService.Models;

namespace Coesolutions.Modules.NOBRADataService.Services
{
    public class MainController: ControllerBase
    {
        #region "Web Methods"


       [DnnAuthorize()]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent("<html><body>Hello World</body></html>");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }


       [DnnAuthorize()]
       [HttpGet]
       public List<Note> NotesByType(string NoteType)
       {
           return sqlNotesByType(NoteType);

       }

       [DnnAuthorize()]
       [HttpGet]
       public VTCGroup GetVTCByGroupYear(string group, string year)
       {
        

           DotNetNuke.Services.Exceptions.ModuleLoadException dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
                          string.Format("Received VTC Request for year {0} group {1}", year,group));

           Exceptions.LogException(dex);

           return sqlGetVTCByGroupYear(group, year);


       }


       [DnnAuthorize()]
       [HttpGet]
       public VacationGroup GetVacationsByGroupYear(string group, string year)
       {

           DotNetNuke.Services.Exceptions.ModuleLoadException dex = new DotNetNuke.Services.Exceptions.ModuleLoadException(
               string.Format("Received Vacation Request for year {0} group {1}", year, group));

           Exceptions.LogException(dex);

           return sqlGetVacationsByGroupYear(group, year);


       }



         public class FileData
         {
             public string ContentType { get; set; }
             public string Base64 { get; set; }
             public string FileName { get; set; }
         }


        [AllowAnonymous]
         // [DnnAuthorize()]
         // POST api/savefile
         public HttpResponseMessage Post([FromBody]FileData file)
         {
             var data = Convert.FromBase64String(file.Base64);

             var result = new HttpResponseMessage(HttpStatusCode.OK)
             {
                 Content = new StreamContent(new MemoryStream(data))
             };

             result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

             result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
             {
                 FileName = file.FileName
             };

             return result;
         }

         [DnnAuthorize()]
        //[AllowAnonymous]
        [HttpGet]
        public List<Pilot> GetRotation()
        {

            return sqlGetRotation();

        }


        [DnnAuthorize()]
        //[AllowAnonymous]
        [HttpGet]
        public List<Rundown> GetRundowns()
        {
            return sqlGetRundowns();

        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage getJSONRotation()
        {
            return Request.CreateResponse(HttpStatusCode.OK, sqlGetRotationData(), Configuration.Formatters.JsonFormatter);
        }



        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage getJSONNotes()
        {
            return Request.CreateResponse(HttpStatusCode.OK, sqlGetNotesData(), Configuration.Formatters.JsonFormatter);

        }



        //[DnnAuthorize()]
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage getJSONRundowns()
        {
            return Request.CreateResponse(HttpStatusCode.OK, sqlGetRundownData(), Configuration.Formatters.JsonFormatter);
        }



        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetTestFile()
{
    HttpResponseMessage result = null;
    var localFilePath = HttpContext.Current.Server.MapPath("/DesktopModules/NobraDataService/Images/fireball.jpg");

    if (!File.Exists(localFilePath))
    {
        result = Request.CreateResponse(HttpStatusCode.Gone);
    }
    else
    {
        // Serve the file to the client
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "fireball";                
    }

    return result;
}


        #endregion


        #region "Data Calls"

        protected PsqlConnection conn()
        {
            PsqlConnection con = new PsqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings
                ["NOBRAConnectionString"].ConnectionString);

            return con;

        }

        protected VTCGroup sqlGetVTCByGroupYear(string group, string year)
        {
            VTCGroup intGroup = new VTCGroup(group, year);
            

            String cmdString = cmdGetVTCByGroupYear(group, year);



            PsqlCommand cmd = new PsqlCommand(cmdString, conn());

            cmd.Connection.Open();


            PsqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    VTCList vl = new VTCList();

                    vl.WatchGroup = reader["WatchGroup"].ToString().Substring(0,1)+ "-" + reader["WatchGroup"].ToString().Substring(1,2);

                    vl.StartDate = reader["StartDate"].ToString().Substring(4,2)
                        + "/" + reader["StartDate"].ToString().Substring(6, 2)
                             + "/" + reader["StartDate"].ToString().Substring(0, 4);
//            vacationOrVtcDateFromString(reader["StartDate"].ToString());//vtcDateFromString(year)



                    VTCRecord vr500 = new VTCRecord(reader["C500"].ToString().Trim()
                       ,new DateTime(1900,1,1,7,0,0));

                   vl.VTCRecords.Add(vr500);

                   VTCRecord vr300 = new VTCRecord(reader["C300"].ToString().Trim()
                     , new DateTime(1900, 1, 1, 15, 0, 0));

                   vl.VTCRecords.Add(vr300);

                   VTCRecord vr700 = new VTCRecord(reader["C700"].ToString().Trim()
                                         , new DateTime(1900, 1, 1, 23, 0, 0));

                   vl.VTCRecords.Add(vr700);

                 


                   intGroup.VTClists.Add(vl);


                }


            }

            cmd.Connection.Close();





            return intGroup;
        }

        protected VacationGroup sqlGetVacationsByGroupYear(string group, string year)
        {
            VacationGroup intGroup = new VacationGroup(group, year);


            String cmdString = cmdGetVacationsByGroupYear(group, year);



            PsqlCommand cmd = new PsqlCommand(cmdString, conn());

            cmd.Connection.Open();


            PsqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    VacationList vl = new VacationList();


                    vl.WatchGroup = reader["WatchGroup"].ToString().Substring(0, 1) + "-" + reader["WatchGroup"].ToString().Substring(1, 2);

                    vl.StartDate = reader["StartDate"].ToString().Substring(4, 2)
                        + "/" + reader["StartDate"].ToString().Substring(6, 2)
                             + "/" + reader["StartDate"].ToString().Substring(0, 4);
                    //            vacationOrVtcDateFromString(reader["StartDate"].ToString());//vtcDateFromString(year)

                    Vacation Pilot1 = new Vacation(reader["Pilot_1"].ToString().Trim() );

                    vl.Vacations.Add(Pilot1);

                    Vacation Pilot2 = new Vacation(reader["Pilot_2"].ToString().Trim());

                    vl.Vacations.Add(Pilot2);

                    Vacation Pilot3 = new Vacation(reader["Pilot_3"].ToString().Trim());

                    vl.Vacations.Add(Pilot3);

                    Vacation Pilot4 = new Vacation(reader["Pilot_4"].ToString().Trim());

                    vl.Vacations.Add(Pilot4);
                    Vacation Pilot5 = new Vacation(reader["Pilot_5"].ToString().Trim());

                    vl.Vacations.Add(Pilot5);

                  

                    intGroup.VacationLists.Add(vl);


                }


            }

            cmd.Connection.Close();





            return intGroup;
        }


        List<Note> sqlNotesByType(string NoteType , bool formatHTML = false)
        {
            if (NoteType == null)
            {
                NoteType = "%";
            }
            else
            {
                NoteType = NoteType.Replace("^", "'");
            }



            List<Note> intList = new List<Note>();

            String cmdString = "select * from Notes ";

            if (NoteType == "%")
            {
                cmdString = cmdString + String.Format(@" WHere Note_type LIKE '{0}' ORDER BY Priority,Note_Type,Date DESC ", NoteType);


            }
            else
            {
                cmdString = cmdString + String.Format(@" WHere Note_type IN ('{0}') ORDER BY Priority,Note_Type,Date DESC ", NoteType);


            }



            PsqlCommand cmd = new PsqlCommand(cmdString, conn());

            cmd.Connection.Open();


            PsqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Note n = new Note();
                    try
                    {
                        n.Priority = Convert.ToInt32(reader["Priority"]);
                    }
                        catch (Exception)
                    {

                        n.Priority = 99;
                    }
                    try
                    {
                        n.NoteType = reader["Note_type"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        n.NoteType = NoteType;
                    }
                    try
                    {
                        n.Note1 = reader["Note1"].ToString();

                        if (formatHTML)
                        {
                            n.Note1 = n.Note1.Replace("\r\n", "</br>");
                        }
                    }
                    catch (Exception)
                    {

                        n.Note1 = "";
                    }
                    try
                    {
                        n.NoteDate = Convert.ToDateTime(reader["Date"]);
                    }
                    catch (Exception)
                    {

                        n.NoteDate = new DateTime();
                    }
                    try
                    {
                        n.LastChangeBy = reader["Last_Change_By"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        n.LastChangeBy = "";
                    }



                    intList.Add(n);

                }
            }
            else
            {
                Note n = new Note();
                n.Note1 = "None Found Check back later";

                intList.Add(n);
            }

            cmd.Connection.Close();

            return intList;




        }

        protected DateTime vacationOrVtcDateFromString(string strDate)
        {
            int intYear = Convert.ToInt32(strDate.Substring(0,4));
            int intMonth = Convert.ToInt32(strDate.Substring(4,2));
            int intDay = Convert.ToInt32(strDate.Substring(6,2));


            DateTime intDate = new DateTime(intYear, intMonth, intDay);


            return intDate;
        }

        protected Rotations sqlGetRotationData()
        {
            Rotations r = new Rotations();

            r.data = sqlGetRotation();

            return r;

        }

        protected Notes sqlGetNotesData()
        {
            Notes n = new Notes();



            n.data = sqlNotesByType("%", true);

            return n;


        }


        protected Rundowns sqlGetRundownData()
        {
            Rundowns rd = new Rundowns();



            rd.data = sqlGetRundowns();


            return rd;


        }

        protected List<Rundown> sqlGetRundowns()
        {
            List<Rundown> intList = new List<Rundown>();


            PsqlCommand cmd = new PsqlCommand(cmdGetRundown(), conn());

            cmd.Connection.Open();


            PsqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                int i = 1;

                while (reader.Read())
                {
                    Rundown r = new Rundown();

                    try
                    {
                        r.LogID = Convert.ToInt32(reader["Log_ID"]);

                    }
                    catch (Exception)
                    {

                        r.LogID = 0;
                    }

                    try
                    {
                        r.Status = reader["TurnStatus"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.Status = "X";
                    }

                    try
                    {
                        r.JobDate = Convert.ToDateTime(reader["EstDateOnBoard"]);
                    }
                    catch (Exception)
                    {

                        r.JobDate = new DateTime(1900, 1, 1);
                    }


                    try
                    {
                        r.CallSign = reader["CallSign"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.CallSign = "XXX";
                    }

                    try
                    {
                        r.VesselName = reader["VesselName"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.VesselName = "Error";
                    }

                    try
                    {
                        r.MileDescFrom = reader["FromMileageCode"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.MileDescFrom = "";
                    }


                    try
                    {
                        r.MileCodeFrom = Convert.ToDecimal(reader["MileNumFrom"]);
                    }
                    catch (Exception)
                    {
                        
                        r.MileCodeFrom = 0;
                    }

                     try
                    {
                        r.MileDescTo = reader["ToMileageCode"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.MileDescTo = "";
                    }


                    try
                    {
                        r.MileCodeTo = Convert.ToDecimal(reader["MileNumTo"]);
                    }
                    catch (Exception)
                    {
                        
                        r.MileCodeTo = 0;
                    }

                    try
                    {
                        r.TimeEstimate = reader["TimeEstimate"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.TimeEstimate = "???";
                    }

                    try
                    {
                        r.DWT = Convert.ToInt32(reader["VesselDwt"]);
                    }
                    catch (Exception)
                    {

                        r.DWT = 0;
                    }

                    try
                    {
                        int feet = Convert.ToInt32(reader["RundownDraftFeet"]);


                        int inches = Convert.ToInt32(reader["RundownDraftInches"]);

                        if (feet > 0)
                        {
                            r.DraftString = string.Format(@"{0}'{1}""", feet.ToString(), inches.ToString());

                        }
                        else
                        {
                            r.DraftString = "";
                        }



                    }
                    catch (Exception)
                    {

                        r.DraftString = "";
                    }

                    try
                    {
                        int feet = Convert.ToInt32(reader["AirDraftFeet"]);


                        int inches = Convert.ToInt32(reader["AirDraftInches"]);

                        if (feet > 0)
                        {
                            r.AirDraftString = string.Format(@"{0}'{1}""", feet.ToString(), inches.ToString());

                        }
                        else
                        {
                            r.AirDraftString = "";
                        }



                    }
                    catch (Exception)
                    {

                        r.AirDraftString = "";
                    }


                    try
                    {
                        r.LOAFeet = Convert.ToDecimal(reader["VesselLoa"]);
                    }
                    catch (Exception)
                    {

                        r.LOAFeet = 0;
                    }

                    try
                    {
                        r.OrderAgentCode = reader["RundownOrderAgentCode"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.OrderAgentCode = "";
                    }

                    try
                    {
                        r.PilotName = reader["PilotName"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.PilotName = "";
                    }

                    //,,,        
                    try
                    {
                        string hivCrewYN = reader["HIVCrewYN"].ToString().Trim();
                        string hivCargoYN = reader["HIVCargoYN"].ToString().Trim();
                        string hivOtherYN = reader["HIVOtherYN"].ToString().Trim();

                        r.HIVInfo = "";
                        r.HIV = 0;
                        if (hivCrewYN.ToUpper().Contains("Y"))
                        {
                            r.HIV = 1;
                            r.HIVInfo = String.Format(@"{0} {1} </br>", r.HIVInfo, reader["HIVCrewDesc"].ToString().Trim());
                        }

                        if (hivCargoYN.ToUpper().Contains("Y"))
                        {
                            r.HIV = 1;
                            r.HIVInfo = String.Format(@"{0} {1} </br>", r.HIVInfo, reader["HIVCargoDesc"].ToString().Trim());
                        }

                        if (hivOtherYN.ToUpper().Contains("Y"))
                        {
                            r.HIV = 1;
                            r.HIVInfo = String.Format(@"{0} {1} </br>", r.HIVInfo, reader["HIVOtherDesc"].ToString().Trim());
                        }



                    }
                    catch (Exception)
                    {

                        r.HIV = 0;
                        r.HIVInfo = "";
                    }

                    try
                    {
                        r.Notes = reader["RundownNotes"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.Notes = "";
                    }

                    //, Tug_ID_From,Tugs_From,Tug_ID_To,Tugs_To
                    try
                    {
                        r.TugIDFrom = reader["Tug_ID_From"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.TugIDFrom = "";
                    }

                    try
                    {
                        r.TugCountFrom = Convert.ToInt32(reader["Tugs_From"]);
                    }
                    catch (Exception)
                    {

                        r.TugCountFrom = 0;
                    }

                    try
                    {
                        r.TugIDTo = reader["Tug_ID_To"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        r.TugIDTo = "";
                    }

                    try
                    {
                        r.TugCountTo = Convert.ToInt32(reader["Tugs_To"]);
                    }
                    catch (Exception)
                    {

                        r.TugCountTo = 0;
                    }

                    try
                    {
                        r.IMO = reader["VesselImo"].ToString();
                    }
                    catch (Exception)
                    {

                        r.IMO = "000";
                    }

                    try
                    {
                        r.LastChangeBy = reader["LastChange"].ToString().Trim();

                    }
                    catch (Exception)
                    {

                        r.LastChangeBy = "";
                    }

                    try
                    {
                        r.LastChangeDate = Convert.ToDateTime(reader["ChangeDate"])
                            + Convert.ToDateTime(reader["ChangeTime"]).TimeOfDay;
                    }
                    catch (Exception)
                    {

                        r.LastChangeDate = new DateTime(1900, 1, 1);
                    }






                    intList.Add(r);
                }
            }
            else
            {

                Rundown r = new Rundown();



                intList.Add(r);
            }

            cmd.Connection.Close();



            return intList;
        }

        protected List<Pilot> sqlGetRotation()
        {
            List<Pilot> intList = new List<Pilot>();


             PsqlCommand cmd = new PsqlCommand(cmdGetRotation(), conn());

            cmd.Connection.Open();


            PsqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                int i = 1;

                while (reader.Read())
                {

                    Pilot p = new Pilot();
                    p.RowNumber = i;
                    i++;

                    try
                    {
                        p.PilotCode = Convert.ToInt32(reader["PilotCode"]);
                    }
                    catch (Exception)
                    {

                        p.PilotCode = 0;
                    }

                    try
                    {
                        p.Name = reader["PilotName"].ToString();
                    }
                    catch (Exception)
                    {

                        p.Name = "Error";
                    }
                    try
                    {
                        p.Phone1 = "+15048858686";
                        //p.Phone1 = reader["PilotPhone"].ToString().Replace("(","").Replace("(","").Replace("-","");
                    }
                    catch (Exception)
                    {

                        p.Phone1 = "";
                    }
                    try
                    {
                        p.Restrictions = reader["SpecialPilotRequest"].ToString();
                    }
                    catch (Exception)
                    {

                        p.Restrictions = "";
                    }
                   




                    intList.Add(p);
                }
            }
            else
            {
                Pilot p = new Pilot();
                p.Name = "None Found";
                intList.Add(p);
            }

            cmd.Connection.Close();



            return intList;


        }


    
        #endregion

        
        #region "SQL Queries"

        /// <summary>
        /// The Calendar is harcoded until 2016 this. This method sets date which is a string value
        /// </summary>
        /// <param name="group"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string cmdGetVTCByGroupYear(string group, string year)
        {
            string yearWhere = vacationDateFromString(year);

            return String.Format(@"select TOP 26 * 
            from VtcSchedule
            WHERE StartDate >= '{0}'
            AND Watch = '{1}'
            ORDER BY StartDate,Watch, WatchGroup ",yearWhere,group);




        }

        private static string vacationDateFromString(string year)
        {
            string yearWhere = "00000000";

            //2016,'01/13/2016'DATE,
            //2017,'01/11/2017'DATE,
            //2018,'01/10/2018'DATE,
            //2019,'01/09/2019'DATE,
            //2020,'01/08/2020'DATE,
            //2021,'01/06/2021'DATE,
            //2022,'01/05/2022'DATE,
            //2023,'01/04/2023'DATE,
            //2024,'01/03/2024'DATE,
            //2025,'01/01/2025'DATE,
            //2026,'12/31/2025'DATE,


            switch (year)
            {
                case "2016":
                    yearWhere = "20160113";
                    break;
                case "2017":
                    yearWhere = "20170111";
                    break;
                case "2018":
                    yearWhere = "20180110";
                    break;
                case "2019":
                    yearWhere = "20190109";
                    break;
                case "2020":
                    yearWhere = "20200108";
                    break;
                case "2021":
                    yearWhere = "20210106";
                    break;
                case "2022":
                    yearWhere = "20220105";
                    break;
                case "2023":
                    yearWhere = "20230104";
                    break;
                case "2024":
                    yearWhere = "20240103";
                    break;
                case "2025":
                    yearWhere = "20260101";
                    break;
                case "2026":
                    yearWhere = "20251231";
                    break;




            }
            return yearWhere;
        }

        /// <summary>
        /// The Calendar is harcoded until 2016 this. This method sets date which is a string value
        /// </summary>
        /// <param name="group"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string cmdGetVacationsByGroupYear(string group, string year)
        {
            string yearWhere = vtcDateFromString(year);

            return String.Format(@"select TOP 26 * 
            from VacationSchedule
            WHERE StartDate >= '{0}'
            AND Watch = '{1}'
            ORDER BY StartDate,Watch, WatchGroup ", yearWhere, group);


        }



        protected string cmdPilotsCore()
        {
            return @"SELECT Pilot_Code as PilotCode, PilotName, Special_Pilot_Reques as SpecialPilotRequest
                , RestrictedYN as RestrictedYesNo
                , RestrictedLOA as RestrictedLoa, Status, AssignedYN as AssignedYesNo, Pilots.Notes AS PilotNotes,
                ExtraDayOfferedOrCom,ExtraTimeOfferedOrCo,GroupAB as GroupAb,Extra_Board_Last_Res
                , RestrictedDWT as RestrictedDwt
                , RestrictedDraft, Phone1 as PilotPhone,ExtraDayFinished, Last_Finish_Date as LastFinishDate, 
                Last_Finish_Time as LastFinishTime,Last_Order_Date as LastOrderDate,Last_Order_Time as LastOrderTime,
                ExtraTimeFinished, Last_Assigned_Date as LastAssignedDate,Last_Assigned_Time as LastAssingedTime
                ,Filler1 as AssignedApprentice,
                Phone1, Description1 as Phone1Description, Phone2, Description2 as Phone2Description
                , Phone3, Description3 as Phone3Description
                FROM Pilots ";


        }

        protected string cmdRundownCore()
        {
            return @"SELECT  Log_ID,Turn_Status_Pending as TurnStatus, Est__Date_on_Board as EstDateOnBoard, Time_Estimate as TimeEstimate
                , Call_Sign as CallSign, Vessel_Name as VesselName , Draft_Footage as RundownDraftFeet, Draft_Inches as RundownDraftInches
                , MileNumFrom,From_Mileage_Code as FromMileageCode, MileNumTo, To_Mileage_Code as ToMileageCode
                ,ChangeDate, ChangeTime, LastChange, Apprentice1 as AssignedApprentice
                ,Apprentice2 as AssignedApprentice2,Apprentice3 as AssignedApprentice3,AirDraftFt as AirDraftFeet, AirDraftInches
                , Rundown_Table.Pilot_Code as PilotCode, Rundown_Table.Notes as RundownNotes, Order_Agent_Code as RundownOrderAgentCode
                ,Assigned_Date as AssignedDate, Assigned_Time as AssignedTime
                ,Vessels.Restriction as VesselRestriction, Vessels.DWT as VesselDwt, Vessels.LOAFeet as VesselLoa, Vessels.Type as VesselType
                ,Vessels.BredthFeet as VesselBredthFeet, Vessels.Flag as VesselFlag, Vessels.VesselNotes,Vessels.LR_IMO_Ship_Number as VesselImo
                ,PilotsTable.PilotName, PilotsTable.Phone1 as PilotPhone,Agent_Codes.AgentName, Agent_Codes.Phone as AgentPhone
                , Agent_Codes.Phone_1 AS AgentPhone1, Agent_Codes.Cell_1 AS AgentCell1, Agent_Codes.Pager_1 AS AgentPager1
                ,CodesFrom.Phone_Notes as CodesPhoneNotesFrom, CodesFrom.Description1 as CodesDescriptionFrom
                ,CodesTo.Phone_Notes as CodesPhoneNotesTo, CodesTo.Description1 as CodesDescriptionTo
 ,Rundown_Table.HIVCrewYN , Rundown_Table.HIVCrewDesc,Rundown_Table.HIVCargoYN,Rundown_Table.HIVCargoDesc,Rundown_Table.HIVOtherYN,Rundown_Table.HIVOtherDesc                
, Tug_ID_From,Tugs_From,Tug_ID_To,Tugs_To

FROM Rundown_Table 
                INNER JOIN Pilots as PilotsTable ON Rundown_Table.Pilot_Code = PilotsTable.Pilot_Code
                INNER JOIN Vessels ON Call_Sign=Vessels.CallSign
                INNER JOIN Agent_Codes ON Order_Agent_Code = Agent_Codes.AgentCode
                INNER JOIN Mileage_Codes As CodesFrom ON From_Mileage_Code = CodesFrom.MileCode
                INNER JOIN Mileage_Codes As CodesTo ON To_Mileage_Code = CodesTo.MileCode
                ";




        }

        protected string cmdGetRundown()
        {

            return string.Format(@" {0} {1}
                ORDER BY Turn_Status_Pending
                ,EstDateOnBoard,Time_Estimate", cmdRundownCore(), " WHERE Turn_Status_Pending IN ('A','P','O') ");




        }



        protected string cmdGetRotation()
        {

            return String.Format("{0} WHERE Status = '{1}' AND AssignedYN = '{2}' ORDER BY Last_Finish_Date,  Last_Finish_Time ", cmdPilotsCore(), "W", "N");


        }

        private static string vtcDateFromString(string year)
        {
            string yearWhere = "00000000";

            //2016,'01/13/2016'DATE,
            //2017,'01/11/2017'DATE,
            //2018,'01/10/2018'DATE,
            //2019,'01/09/2019'DATE,
            //2020,'01/08/2020'DATE,
            //2021,'01/06/2021'DATE,
            //2022,'01/05/2022'DATE,
            //2023,'01/04/2023'DATE,
            //2024,'01/03/2024'DATE,
            //2025,'01/01/2025'DATE,
            //2026,'12/31/2025'DATE,


            switch (year)
            {
                case "2016":
                    yearWhere = "20151209";
                    break;
                case "2017":
                    yearWhere = "20161207";
                    break;
                case "2018":
                    yearWhere = "20171206";
                    break;
                case "2019":
                    yearWhere = "20181205";
                    break;
                case "2020":
                    yearWhere = "20191204";
                    break;
                case "2021":
                    yearWhere = "20201202";
                    break;
                case "2022":
                    yearWhere = "20211201";
                    break;
                case "2023":
                    yearWhere = "20221130";
                    break;
                case "2024":
                    yearWhere = "20231129";
                    break;
                case "2025":
                    yearWhere = "20241127";
                    break;
                case "2026":
                    yearWhere = "20251126";
                    break;

            }
            return yearWhere;
        }

        #endregion


        #region "Helpers"
        protected string ProperCase(string input)
        {

            // Defines the string with mixed casing.
            string myString = input;                                       //; "5CARNIVAL LEGEND" //[5]    "116-SHAFFER, D. L. "

            // Creates a TextInfo based on the "en-US" culture.
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

            // Changes a string to lowercase.
            int plevel;
            bool IsNumeric = int.TryParse(myString.Substring(0, 1), out plevel);
            //try
           



            string inString = myTI.ToLower(myString);
            string outString = string.Empty;
            char[] chars = inString.ToCharArray();

            //Always try upper start
            string subString = chars[0].ToString();


            subString = myTI.ToUpper(subString);

            char[] subChars = subString.ToCharArray();

            chars[0] = subChars[0];


            //Loop through once to find things for upper case
            if (IsNumeric)
            {
                subString = chars[1].ToString();


                subString = myTI.ToUpper(subString);

                subChars = subString.ToCharArray();

                chars[1] = subChars[0];

            }


            for (int i = 1; i < chars.Length; i++)
            {

                //Name will Follow Pilot number
                if (chars[i].ToString() == "-" || chars[i].ToString() == "(")
                {

                    subString = chars[i + 1].ToString();


                    subString = myTI.ToUpper(subString);

                    subChars = subString.ToCharArray();

                    chars[i + 1] = subChars[0];
                }

                //Space marks a new word
                if ((chars[i].ToString() == " " && (i + 1) < chars.Length))
                {

                    subString = chars[i + 1].ToString();


                    subString = myTI.ToUpper(subString);

                    subChars = subString.ToCharArray();

                    chars[i + 1] = subChars[0];
                }


                //Marks an initial
                if (chars[i].ToString() == ".")
                {

                    subString = chars[i - 1].ToString();


                    subString = myTI.ToUpper(subString);

                    subChars = subString.ToCharArray();

                    chars[i - 1] = subChars[0];
                }



            }
            for (int i = 0; i < chars.Length; i++)
            {
                outString = outString += chars[i].ToString();
            }


            outString = outString.Replace("Atb", "ATB");


            return outString;




        }




        #endregion










    }
}
