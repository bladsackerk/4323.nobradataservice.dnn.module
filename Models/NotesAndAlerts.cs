﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.NOBRADataService.Models
{
    public class Notes
    {
        public List<Note> data { get; set; }



    }


    public class Note
    {
        public int Priority { get; set; }
        public string NoteType { get; set; }
        public string Note1 { get; set; }
        public DateTime NoteDate { get; set; }
        public string LastChangeBy { get; set; }
    }


}
