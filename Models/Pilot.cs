﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.NOBRADataService.Models
{
    public class Rotations
    {
        public List<Pilot> data { get; set; }


    }

    public class Pilot
    {
        public int RowNumber { get; set; }
        public int PilotCode { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Restrictions { get; set; }
    }



}
