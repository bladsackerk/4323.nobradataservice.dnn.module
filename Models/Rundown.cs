﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.NOBRADataService.Models
{
    public class Rundowns
    {
        public List<Rundown> data { get; set; }

    }


    public class Rundown
    {
        public int LogID { get; set; }
        public string Status { get; set; }
        public DateTime JobDate { get; set; }
        public string CallSign { get; set; }
        public string VesselName { get; set; }
        public string MileDescFrom { get; set; }
        public decimal MileCodeFrom { get; set; }
        public string MileDescTo { get; set; }
        public decimal MileCodeTo { get; set; }
        public string TimeEstimate { get; set; }
        public int DWT { get; set; }
        public string DraftString { get; set; }
        public string AirDraftString { get; set; }
        public decimal LOAFeet { get; set; }
        public string OrderAgentCode { get; set; }
        public string PilotName { get; set; }
        public int HIV { get; set; }
        public string HIVInfo { get; set; }
        public string Notes { get; set; }
        public string TugIDFrom { get; set; }
        public int TugCountFrom { get; set; }
        public string TugIDTo { get; set; }
        public int TugCountTo { get; set; }
        public string IMO { get; set; }
      
        public string LastChangeBy { get; set; }
        public DateTime LastChangeDate { get; set; }
    }
}
