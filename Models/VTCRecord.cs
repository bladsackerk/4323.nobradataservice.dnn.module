﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.NOBRADataService.Models
{

    public class VTCRecord
    {
        public VTCRecord(string pName, DateTime vDate)
        {
            PilotName = pName;
            VtcTime = vDate;
        }

        public string PilotName { get; set; }
        public DateTime VtcTime { get; set; }
    }

    public class VTCList
    {
        public VTCList() 
        {
            VTCRecords = new List<VTCRecord>(); 
        }

        public VTCList(string wg
           , string dt
           )
        {
            WatchGroup = wg;
            StartDate = dt;
            VTCRecords = new List<VTCRecord>();



        }

        public VTCList(string wg
            , string dt
            , List<VTCRecord> vrs)
        {
            WatchGroup = wg;
            StartDate = dt;
            VTCRecords = vrs;



        }

        public string WatchGroup { get; set; }
        public string StartDate { get; set; }
        public List<VTCRecord> VTCRecords  { get; set; }

    }

    public class VTCGroup
    {
        public VTCGroup() 
        { 
            VTClists = new List<VTCList>();
        
        }

        public VTCGroup(string gp, string yr)
        {
            Group = gp;
            Year = yr;
            VTClists = new List<VTCList>();


        }


        public string Group { get; set; }
        public string Year { get; set; }
        public List<VTCList> VTClists { get; set; }
    }




}
