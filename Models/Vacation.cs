﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.NOBRADataService.Models
{
    public class Vacation
    {
        public Vacation() { }

        public Vacation(string pname)
        {
            PilotName = pname;
        }
        public string PilotName { get; set; }
    }

    public class VacationList
    {
        public VacationList() 
        {
            Vacations = new List<Vacation>();
        }

        public VacationList(string wg
           , string dt
           )
        {
            WatchGroup = wg;
            StartDate = dt;
            Vacations = new List<Vacation>();
        }


        public string WatchGroup { get; set; }
        public string StartDate { get; set; }
        public List<Vacation> Vacations { get; set; }
    }

    public class VacationGroup 
    {
        public VacationGroup() 
        {
            VacationLists = new List<VacationList>();
        }

        public VacationGroup(string gp, string yr)
        {
            Group = gp;
            Year = yr;
            VacationLists = new List<VacationList>();
        }

        public string Group { get; set; }
        public string Year { get; set; }
        public List<VacationList> VacationLists { get; set; }
    
    }


}
